package com.omgcode.kafkamq;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaListeners {

    @KafkaListener(
            topics = "omgTopic",
            groupId = "groupId"
    )
    public void listeners(String data) {
        System.out.println("listener received: " + data);
    }
}
